<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('operator')->delete();
        DB::table('operator')->insert([
            [
                'kode_operator' => '1',
                'nama' => 'Rivaldo',
                'password' => bcrypt('user'),
            ],

            [
                'kode_operator' => '2',
                'nama' => 'Iqbal',
                'password' => bcrypt('user'),
            ],

            [
                'kode_operator' => '3',
                'nama' => 'Jefri',
                'password' => bcrypt('user'),
            ]

        ]);
    }
}
